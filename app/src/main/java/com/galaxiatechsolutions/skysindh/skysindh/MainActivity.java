package com.galaxiatechsolutions.skysindh.skysindh;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void button(View view)
    {
        // Open Website

        Intent intent;

        try {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://skysindh.com"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            Log.e("Exception Caught", e.toString());
        }
    }
}
